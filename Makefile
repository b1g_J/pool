# Makefile for Pool

TEST_BIN=./bin/test
OPTIONS=-Wall --std=c++11
INCLUDES=-I./include/ -I./src/
LIBS=-pthread

all: Pool.o test.o test
clean:
	rm bin/Pool.o
	rm bin/test.o
	rm bin/test

Pool.o: 
	g++ $(OPTIONS) $(INCLUDES) -c -o bin/Pool.o src/Pool.cpp

test.o: 
	g++ $(OPTIONS) $(INCLUDES) -c -o bin/test.o src/test.cpp

test: Pool.o test.o 
	g++ $(OPTIONS) -o $(TEST_BIN) bin/Pool.o bin/test.o $(LIBS)
