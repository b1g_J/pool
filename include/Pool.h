

/**

*/

#ifndef Pool_H
#define Pool_H

#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <mutex>

#define VERBOSE_THREAD_POOL true
#define THRESHOLD 10

template <typename ARG_TYPE=int, typename R=int>
class Pool {
    typedef R (*F)(ARG_TYPE);
    struct Task {
        F func;
        ARG_TYPE arg;
        R* returnPointer;
    };
 public:
    Pool(int processes);//, F func);

    std::vector<R> map(F func, std::vector<ARG_TYPE> argv);
 private:
    int processes;
    //F func;
    std::mutex indexMutex;
};

template class Pool<int, int>;

#endif
