#include "Pool.h"

using namespace std;
using namespace jmc;

template <typename ARG, typename RET> 
vector<RET> Pool::map(RET (*func)(ARG), const vector<ARG>& argv) {
    size_t taskIndex = 0, argc = argv.size();
    
    vector<RET> returnValues (argc);

    auto threadLoop = [&] () {
        size_t i=0;
        while (taskIndex < argc) {
            indexMutex.lock();
            i=taskIndex++;
            indexMutex.unlock();

            if (i >= argc)
                break;

            returnValues[i] = func(argv[i]);
        }
    };

    vector<thread> threads;
    for (int proc=0; proc<processes; ++proc) {
        threads.push_back(thread(threadLoop));
    }
    
    for (thread& t : threads)
        t.join();

    return returnValues;
}


template <typename ARG, typename RET> 
vector<RET> Pool::verboseMap(RET (*func)(ARG), const vector<ARG>& argv) {
    size_t taskIndex = 0, argc = argv.size();
    
    vector<RET> returnValues (argc);
    bool activeSpinner = true;

    auto threadLoop = [&] () {
        size_t i=0;
        while (taskIndex < argc) {
            indexMutex.lock();
            i=taskIndex++;
            indexMutex.unlock();

            if (i >= argc)
                break;

            returnValues[i] = func(argv[i]);
        }
    };

    auto spinnerFunc = [&activeSpinner] () {
        char spinner[] = "\\|/-";
        int index = 0;
        
        while (activeSpinner) {
            cout << spinner[index] << '\r';
            cout.flush();
            index = (index + 1) % 4;
            this_thread::sleep_for(chrono::milliseconds(250));
        }
        
    };

    vector<thread> threads;
    for (int proc=0; proc<processes; ++proc) {
        threads.push_back(thread(threadLoop));
    }

    thread spinnerThread(spinnerFunc);
    
    for (thread& t : threads)
        t.join();
    
    activeSpinner = false;
    spinnerThread.join();

    return returnValues;
}
