#include "Queue.h"

#define DEFAULT_CAPACITY 10

using namespace std;

template <class T>
Queue<T>::Queue(size_t n): capacity(n), size(0), front(0) {    
    data = new T[n];
}

template <class T>
Queue<T>::Queue(T data[], size_t n): capacity(n), size(sizeof(data) / sizeof(T)), front(0) {
    this->data = new T[n];
    for (int index=0; index<size; ++index) {
        this->data[index] = new T(data[index]);
    }
}

template <class T>
Queue<T>::~Queue() {
    delete[] data;
}

template <class T>
Queue<T>::Queue(const Queue& q) {
    data = new T[q.capacity];
    for (int index=0; index<capacity; ++index) {
        data[index] = new T(q.data[index]);
    }
}

template <class T>
Queue<T>::Queue(Queue&& q) {
    data = q.data;
    for (int index=0; index<q.capacity; ++index) {
        q.data = nullptr;
    }
}

template <class T>
void Queue<T>::push(T& object) {
    if (size == capacity - 1) {
        resize(size * 2);
    }
    size_t index = (front + size++) % capacity;
    data[index] = T(object);
}

template <class T>
T Queue<T>::pop() {
    if (size == (capacity / 2) + 1 && size > 10) {
        resize(size / 2);
    }
    size_t index = front;
    front = (front + 1) % capacity;
    T item = data[index];
    return item;
}

template <class T>
void Queue<T>::resize(size_t new_capacity) {
    if (new_capacity < size) {
        cerr << "invalid capacity" << endl;
        exit(1);
    }
    T* new_data = new T[new_capacity];
    for (size_t index=0; index<size; ++index) {
         new_data[index] = data[(front + index) % capacity];
    }
    front = 0;
    capacity = new_capacity;
    delete[] data;
    data = new_data;
}

template <class T>
typename Queue<T>::iterator Queue<T>::begin() {
    return Queue<T>::iterator(data, capacity, front);
}

template <class T>
const typename Queue<T>::iterator Queue<T>::begin() const {
    return Queue<T>::iterator(data, capacity, front);
}

template <class T>
typename Queue<T>::iterator Queue<T>::end() {
    return Queue<T>::iterator(data, capacity, (front + size) % capacity);
}

template <class T>
const typename Queue<T>::iterator Queue<T>::end() const {
    return Queue<T>::iterator(data, capacity, (front + size) % capacity);
}

template <class T>
Queue<T>::iterator::iterator(T data[], size_t capacity, size_t index) : data(data), capacity(capacity), index(index) {}

template <class T>
T& Queue<T>::iterator::operator++() {
    index = (index + 1) % capacity;
    return data[index];
}

template <class T>
T& Queue<T>::iterator::operator++(int) {
    T item = *(*this);
    this->operator++();
    return item;
}

template <class T>
T& Queue<T>::iterator::operator*() {
    return data[index];
}

template <class T>
bool Queue<T>::iterator::operator!=(const Queue<T>::iterator& iter) {
    return &data[index] != &iter.data[iter.index];
}
