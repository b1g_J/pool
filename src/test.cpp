#include <iostream>
#include <string>
#include "Pool.h"

#define ARG_NUMBER 20

using namespace std;


int main(int argc, char* argv[]) {
    vector<int> args (ARG_NUMBER);
    for (size_t i=0; i<args.size(); ++i)
        args[i] = i;
    
    //    typedef int (*funcType)(int);
    Pool<int, int> pool(5);
    vector<int> argr = pool.map([](int i) -> int { return i; }, args);
    
    for (int i=0;  i<ARG_NUMBER; ++i)
        cout << "argr[" << i << "] = " << argr[i] << endl;
}
